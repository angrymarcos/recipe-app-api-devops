variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "angrymarcos15@gmail.com"
}

variable "db_username" {
  default = "Username for RDS postgres instance."
}

variable "db_password" {
  default = "Password for RDS postgres instance."
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "468637300330.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image proxy"
  default     = "468637300330.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "DNS name"
  default     = "angrymarcos.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api",
    staging    = "api.staging",
    dev        = "api.dev"
  }
}

variable "acl" {
  type        = string
  description = "Defaults to Public-read"
  default     = "public-read"
}
